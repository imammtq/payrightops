#!/bin/bash
# echo -e "#====================================================================================# "
# echo -e  "# Name:         automaticalyDump.sh                                                 #"
# echo -e  "# Description:  automaticaly dump, upload to s3 Bucket and delete spesific tables   #"
# echo -e  "# Version:      V.1.1.0                                                             #"
# echo -e  "# Author     :  DevOps@payrightsystem.com                                           #"
# echo -e  "# License:      GNU General Public License, version 3 (GPLv3)                       #"
# echo -e  "# License URI:  http://www.gnu.org/licenses/gpl-3.0.html                            #"
# echo -e  "#===================================================================================#"

databaseName=payrightdev
usernameDatabase=
passwordDatabase=
tableName=queries_storage

year=$(date +%Y)
month=$(date +%m)
execResult=$?

#nantinya bakal jadi filename nya, sementara bentuk nya bakal yyyy-mm-dd.sql.gz
fileName=$(date +%F).sql.gz
#process ngedump database dengan spesific nama tablenya, hasil nya bakan dalam bentuk zip
execDump=$(mysqldump -u $usernameDatabase -p$passwordDatabase $databaseName $tableName | gzip -c > $fileName)
#link dari s3 bucket, nanti format dir s3 bucket nya bakal s3:://nama s3 bucket/tahun/bulan/namafile.sql.gz
s3BucketLink="s3://queriesstorage/${year}/${month}/${fileName}"
queryDelete='DELETE from queries_storage WHERE create_date < DATE_SUB(NOW(), INTERVAL 3 MONTH)'
${execDump}
    if [ $execResult -eq 0 ]; then
        echo 'Dump tables queries_storage DONE' | mail -s 'Report dump queries_storage database' imam@payrightsystem.com
        #upload hasil dump dabtaase ke s3 bucket
        $(aws s3 cp $fileName $s3BucketLink --profile s3) && 
        echo 'upload backup queries_storage DONE' | mail -s 'Upload queries_storage backup to s3 Bucket Done' imam@payrightsystem.com 
        #menghapus data dari table query_storage yang lebih dari 3 bulan
        mysql -u $usernameDatabase -p$passwordDatabase -D $databaseName -e "$queryDelete" 
        #menghapus file hasil dari backupan
        rm $fileName 
        echo 'Delete tables on Specified time range and delete file DONE' | mail -s 'Tables success delete and filename deteled' imam@payrightsystem.com
    else
        echo 'Dump queries_storage FAILED' | mail -s 'Report dump queries_storage database' imam@payrightsystem.com
    fi