#!/bin/bash 
echo -e "#==========================================================================#"
echo -e  "# Name        :                  alertmemory.sh                           #"
echo -e  "# Description :  Send alert mail when server memory is running low        #"      
echo -e  "# Version     :            V.Karya Anak Bangsa.1.1                        #"
echo -e  "# Author      :           DevOps@payrightsystem.com                       #"
echo -e  "# License     :      GNU General Public License, version 3 (GPLv3)        #"
echo -e  "# License URI :     http://www.gnu.org/licenses/gpl-3.0.html              #"
echo -e  "#=========================================================================#"

## Declare mail variables
##email subject 
subject="Server Memory Status Alert"
## sending mail to
to="imam@payrightsystem.com"
## send carbon copy to
#also_to="irvandy@payrightsystem.com"

## get total free memory size in megabytes(MB) 
free=$(free -mt | grep Total | awk '{print $4}')

## check if free memory is less or equals to  100MB
if [[ "$free" -le 100  ]]; then
        ## get top processes consuming system memory and save to temporary file 
        ps -eo pid,ppid,cmd,%mem,%cpu --sort=-%mem | head >/tmp/top_proccesses_consuming_memory.txt

        file=/tmp/top_proccesses_consuming_memory.txt
        ## send email if system memory is running low
        echo -e "Warning BOS, server memory is running low!\n\nFree memory: $free MB" | mailx -a "$file" -s "$subject" -r "$from" -c "$to" #"$also_to"
fi

exit 0